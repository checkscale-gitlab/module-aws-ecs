resource "random_string" "this" {
  length  = 8
  upper   = false
  special = false
  number  = false
}

resource "random_integer" "this" {
  max = 100
  min = 1
}

locals {
  prefix = random_string.this.result
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.14"

  name = format("%stest_vpc", local.prefix)
  cidr = "10.${random_integer.this.result}.0.0/16"

  azs             = compact(slice(concat(sort(data.aws_availability_zones.current.names), ["", "", "", ""]), 0, 3))
  private_subnets = ["10.${random_integer.this.result}.0.0/24"]
  public_subnets  = ["10.${random_integer.this.result}.1.0/24"]

  enable_dns_hostnames = true
  enable_dhcp_options  = true
  enable_nat_gateway   = true

  tags = {
    managed-by  = "terraform"
    Environment = "lab"
  }
}

resource "aws_security_group" "efs" {
  name        = "allow efs"
  description = "allow efs"
  vpc_id      = module.vpc.vpc_id

  tags = {
    Name = "allow_efs"
  }
}

resource "aws_security_group_rule" "this_cidrs" {
  security_group_id = aws_security_group.efs.id

  type        = "ingress"
  from_port   = 2049
  to_port     = 2049
  protocol    = "tcp"
  description = "NFS"
  cidr_blocks = module.vpc.private_subnets_cidr_blocks
}

module "efs" {
  source = "git::https://gitlab.com/wild-beavers/terraform/module-aws-efs.git?ref=3.1.0"

  subnet_ids = module.vpc.private_subnets

  name                = format("%secs_test_efs", local.prefix)
  security_group_name = format("%secs_test_efs_sg", local.prefix)

  kms_key_alias_name = format("alias/%secs_test_efs_kms", local.prefix)
  kms_key_name       = format("%secs_test_efs_kms", local.prefix)

  security_group_ids = [aws_security_group.efs.id]

  tags = merge(
    {
      origin = "gitlab.com/wild-beavers/terraform/module-aws-efs"
    },
  )

}

module "example" {
  source = "../../"

  cluster_enabled = true

  prefix = random_string.this.result

  cluster_name                = "test"
  cluster_execution_role_name = "cluster_execution_role"
  cluster_capacity_providers  = ["FARGATE"]
  cluster_default_capacity_provider_strategy = [
    {
      capacity_provider = "FARGATE"
    }
  ]
  task_definition_enabled                  = true
  task_definition_requires_compatibilities = ["FARGATE"]
  task_definition_fargate_cpu              = 512
  task_definition_fargate_mem              = 4096
  task_definition_name                     = "task_def_01"

  task_definition_volumes = [{
    name                            = "ubuntu-volume"
    use_docker_volume_configuration = false
    use_efs_volume_configuration    = true
    efs_volume_configuration = {
      file_system_id           = module.efs.id
      use_authorization_config = false
    }
    use_fsx_windows_file_server_volume_configuration = false
  }, ]

  task_definition_runtime_platform = [
    {
      operating_system_family = "LINUX"
      cpu_architecture        = "X86_64"
    }
  ]

  task_definition_container_definitions = [
    {
      name   = format("%subuntu", local.prefix)
      image  = "ubuntu"
      user   = "root"
      cpu    = 10
      memory = 1024
      portMappings = [
        {
          containerPort = 80
          hostPort      = 80
        }
      ]
      interactive       = true
      memoryReservation = 64
      pseudoTerminal    = true
      mountPoints = [{
        containerPath = "/test"
        sourceVolume  = "ubuntu-volume"
      }]
    }
  ]
  task_definition_tags = {
    Name = "test"
  }

  service_name                               = "ecs_service_TEST"
  service_launch_type                        = "FARGATE"
  service_platform_version                   = "1.4.0"
  service_deployment_maximum_percent         = 100
  service_deployment_minimum_healthy_percent = 0
  service_desired_count                      = 1

  service_network_configuration = [
    {
      subnets = module.vpc.private_subnets
    }
  ]
}
